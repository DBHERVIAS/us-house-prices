import requests
from bs4 import BeautifulSoup

r = requests.get("http://www.in2013dollars.com/Housing/price-inflation/1967-to-2019?")
page = BeautifulSoup(r.content)
table = page.find_all('table', class_='regular-data table-striped')

header_row = ['Year', 'USD Value', 'Inflation Rate']
with open('housingprice.csv', 'w') as csv_file:
    writer = csv.writer(csv_file, delimiter=',', lineterminator='\n')
    writer.writerow(header_row)
    # loop through table body rows 
    rows = page.find('table', class_='regular-data table-striped').find_all('td')     
    for i in range(len(rows)-1):
        if i % 3 == 0:        
            cell1 = rows[i].text.replace('\n', '')
        elif i % 3 == 1:
            cell2 = rows[i].text.replace('\n', '').replace('$', '').replace(',', '')
        else:
            cell3 = rows[i].text.replace('\n', '').replace('%', '')
            csv_row = [cell1,cell2, cell3]
            writer.writerow(csv_row)



















