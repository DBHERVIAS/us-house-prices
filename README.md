
## Historical US House Prices Research

### **Prices for Housing, 1967-2019 ($100,000)**


Home prices around the U.S. surged to new high this year. 
According to the U.S. Bureau of Labor Statistics, prices for housing were 760.95% higher in 2019 versus 1967 (a $760,954.44 difference in value).


**Between 2000 and 2019:** 
Housing experienced an average inflation rate of 2.38% per year. This rate of change indicates significant inflation. In other words, housing costing $100,000 in the year 2000 would cost $156,333.32 in 2019 for an equivalent purchase. Compared to the overall inflation rate of 2.09% during this same period, inflation for housing was higher.

### **Outcome**

* Prices have gone up due to greater demand and a drop in supply in the market in many parts of the country, but a lot of areas still haven't reached the levels they were at before the crisis hit. 
* One factor leading to higher prices -- low mortgage costs are luring more prospective buyers into the market.


Housing priced at $100,000 in 1967 → $860,954.44 in 2019

![](https://i.imgur.com/cDoGKyR.png)

*Years with the largest changes in pricing: 1980 (15.69%), 1979 (12.20%), and 1981 (11.51%).*

Source:
http://www.in2013dollars.com/Housing/price-inflation/1967-to-2019?amount=100000